var sql=require("alasql");

function RuleEngine (){
    var service={};

    service.evaluate=function (rules, answers, debug, callback) {
        var fulfilled=false;
        if(rules.statement.length>0 && canEvaluate(rules.statement, answers, debug)){
            rules.statement.some(function (statement) {
                var questionId=statement.if.questionId;
                var expression=getExpression(questionId, answers, statement.if.operator, statement.if.answer, debug);
                statement.and.forEach(function(andItem){
                    expression=expression+" && "+getExpression(andItem.questionId, answers, andItem.operator, andItem.answer, debug);
                });

                if(debug)
                    console.log(expression);

                if(eval(expression)) {
                    fulfilled=true;
                    callback({
                        type:statement.then.type,
                        url:statement.then.url,
                        questionId:statement.then.questionId,
                        fulfilled:fulfilled
                    });
                    return true;
                }
            });
            if(!fulfilled) {
                callback({
                    type:rules.else.type,
                    url:rules.else.url,
                    questionId:rules.else.questionId,
                    fulfilled:fulfilled
                });
            }
        } else {
            callback({
                type:rules.else.type,
                url:rules.else.url,
                questionId:rules.else.questionId,
                fulfilled:fulfilled
            });
        }
    };

    function getAnswerById (questionId, answers, debug) {
        return sql("select answer from ? where questionId="+questionId,[answers])[0].answer;
    }

    //Returns an expression based up statement and answers. Expressions can evaluate strings or numerics
    function getExpression(questionId, answers, operator, answer, debug) {
        if(isNaN(answer)){
            return "'"+getAnswerById(questionId, answers, debug)+"'"+operator+"'"+answer+"'";
        } else {
            return parseInt(getAnswerById(questionId, answers, debug))+""+operator+""+answer;
        }
    }

    //This method determines if all answers are present in order to evaluate the statements supplied; if not, return false
    function canEvaluate (statement, answers, debug) {
        var questionIdList=[];
        statement.forEach(function(s){
            questionIdList.push(s.if.questionId);
            s.and.forEach(function (a){
                questionIdList.push(a.questionId);
            });
        });

        var have=0;
        var needed=questionIdList.length;

        if(debug)
            console.log(questionIdList);

        questionIdList.forEach(function (questionId){
            var temp=sql("select questionId from ? where questionId=?",[answers, questionId]).length;
            if(debug)
                console.log("temp: "+temp);
            if(temp>0){
                have=have+1;
            }
        });

        if(debug)
            console.log("Do you have all the questions and answers needed to evaluate? - "+eval(have==needed));

        return eval(have==needed);
    }

    return service;
}

module.exports=RuleEngine();