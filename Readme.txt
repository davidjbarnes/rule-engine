A simple rule engine.

A sample rule set:
{
    "statement": [{
		"if": {
			"answer": "yes",
			"operator": "==",
			"questionId": 1
		},
		"and": [],
		"then": {
			"url": "/question/3",
			"type": "question",
			"questionId": 3
		}
	}],
	"else": {
		"url": "/result",
		"type": "result",
		"questionId": null
	}
}

Sample rule set with a single AND:
{
    "statement": [{
		"if": {
			"answer": "yes",
			"operator": "==",
			"questionId": 1
		},
		"and": [{
			"answer": "maybe",
			"operator": "!=",
			"questionId": 2
		}],
		"then": {
			"url": "/question/3",
			"type": "question",
			"questionId": 3
		}
	}],
	"else": {
		"url": "/result",
		"type": "result",
		"questionId": null
	}
}